#!/bin/bash
if [ ! -d "$1" ]
then
  echo "Usage: `basename $0` <path to mozilla-central repo>"
  exit 1
fi
pushd $1
./mach tb-rust check-upstream > ~/mozilla/tb-rust.out || (popd; exit 1)
if grep 'Rust dependencies are okay.' ~/mozilla/tb-rust.out
then
  popd
  exit 0
else 
  cargo update || (popd; exit 1)
  ./mach tb-rust vendor || (popd; exit 1)
fi
pushd comm
hg diff >~/mozilla/tb-rust.patch
popd
cargo update || (popd; exit 1)
popd
