#!/bin/bash
EXIT=0
for i in */patches
do
  check_series.sh $i || EXIT=1
done
if [ "$1" != "" ]
then
  if [ $EXIT -eq 1 ]
  then
    mailx -s "check-series failure in SeaMonkey $1" wag-page,billiam < /dev/null
  fi
fi
exit $EXIT
