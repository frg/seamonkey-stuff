#!/bin/perl
$VERSION="57a1";
$DEBUG=true;

$LAST_BUG="";

$PREFIX="# " if $DEBUG;

while (<>) {
    chomp;
    $oldname=$_;
    open(PATCH, "<$oldname")  || die "Couldn't open file $oldname";
    while (<PATCH>) {
        if (!/^[#] /) {
            if (/^[Bb][Uu][Gg] ([0-9]*)/) {
                $BUG=$1;
                if ($BUG eq $LAST_BUG) {
                    $PATCH+=1;
                }
                else {
                    $LAST_BUG=$BUG;
                    $PATCH=1;
                }
                if ($PATCH == 1) {
                    print "${PREFIX}hg qrename $oldname $BUG-$VERSION.patch\n";
                }
                else {
                    print "${PREFIX}hg qrename $oldname $BUG-$PATCH-$VERSION.patch\n";
                }
                close PATCH;
            }
            elsif (/^servo: Merge #([0-9]*)/) {
                $BUG="servo-$1";
                if ($BUG eq $LAST_BUG) {
                    $PATCH+=1;
                }
                else {
                    $LAST_BUG=$BUG;
                    $PATCH=1;
                }
                if ($PATCH == 1) {
                    print "${PREFIX}hg qrename $oldname $BUG-$VERSION.patch\n";
                }
                else {
                    print "${PREFIX}hg qrename $oldname $BUG-$PATCH-$VERSION.patch\n";
                }
                close PATCH;
            }
            else {
                print STDERR "cannot determine new filename for $oldname\n";
                print "# cannot determine new filename for $oldname\n";
                close PATCH;
            }
        }
    }
}
