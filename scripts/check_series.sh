#!/bin/bash
ERRORS=0
if [ "$1" = "" ]
then
  pushd . >/dev/null
else
  echo Checking $1 ...
  pushd $1 >/dev/null
fi
if [ -f series-test ]
then
  SERIES=series-test
  FILES=`cat series-test`
else
  SERIES=series
  FILES=*
fi
if [ ! -f $SERIES ]
then
  echo no $SERIES file found
  popd >/dev/null
  exit 1
fi
for i in $FILES
do
  if [ "$i" != "series" -a "$i" != "status" ]
  then
    j=`echo $i | sed -e 's@[A-Za-z0-9_.+-]@@g'`
    if [ "$j" != "" ]
    then
      echo Illegal character\(s\) \'$j\' in filename \'$i\'
      echo \'hg header\' and \'non-ascii character\' error checks disabled for this file.
      ERRORS=`expr $ERRORS + 1`
      continue
    fi
    j=`echo $i | sed -e 's@\.@\\\.@g'`
    COUNT=`grep ^$j$ $SERIES | wc | sed -e 's@^\s*@@g' -e 's@ .*$@@g'`
    if [ "$COUNT" != "1" ]
    then
      ERRORS=`expr $ERRORS + 1`
      echo $i appears $COUNT times in $SERIES
    fi
    if file $i | grep -E -v ' ASCII .*text| UTF-8 .*text| HTML .*text| SGML .*text' >/dev/null
    then
#     NUM=3
      NUM=`grep -n '^diff' $i | head -1 | sed -e 's@\:.*$@@'`
      if [ "$NUM" = "" ]
      then
        ERRORS=`expr $ERRORS + 1`
        echo non-ascii characters in $i
      else
        NUM=`expr $NUM - 1`
        head -$NUM $i > /tmp/cs$$
        if file /tmp/cs$$ | grep -E -v ' ASCII .*text| UTF-8 .*text| HTML .*text| SGML .*text' >/dev/null
        then
          ERRORS=`expr $ERRORS + 1`
          echo non-ascii characters in $i
        fi
      fi
    fi
    if head -1 $i | grep -v '^# HG changeset patch$' >/dev/null
    then
      ERRORS=`expr $ERRORS + 1`
      echo bad hg header in $i
    fi
    if ! head -3 $i | grep '^# User .*[@<].*[>A-Za-z0-9]$' >/dev/null
    then
      ERRORS=`expr $ERRORS + 1`
      echo bad hg header in $i
    fi
    if ! head -4 $i | grep '# Date .*[0-9]$' >/dev/null
    then
      ERRORS=`expr $ERRORS + 1`
      echo bad hg header in $i
    fi
    rm -f /tmp/cs$$
  fi
done
for i in `grep -v '^#' $SERIES`
do
  if [ ! -f $i ]
  then
    echo File $i in $SERIES does not exist.
    ERRORS=`expr $ERRORS + 1`
    echo $i
  fi
done
popd >/dev/null
if [ "$ERRORS" = "0" ]
then
  echo no errors detected
  exit 0
else
  if [ "$ERRORS" = "1" ]
  then
    echo 1 error detected
  else
    echo $ERRORS errors detected
  fi
fi
exit 1
